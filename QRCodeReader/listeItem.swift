//
//  listeItem.swift
//  QRCodeReader
//
//  Created by richard urunuela on 20/11/2017.
//  Copyright © 2017 AppCoda. All rights reserved.
//

import UIKit
class MenuItem: NSObject {
    var name = "None"
    var price = 0.00
    var special = false
    override init(){}
    init(name:String,price:Double,special:Bool){
        self.name = name
        self.price = price
        self.special = special
    }
}

class MenuItems:NSObject{
    var sections:[String] = []
    var items:[[MenuItem]] = []
    
    func add(section: String, item:[MenuItem]){
        sections = sections + [section]
        items = items + [item]
    }
}
class MyMenuItems: MenuItems {
    override init() {
        super.init()
        
        add(section:"Imtem", item: [
            MenuItem(name:"item 1",price:7.95,special:false),
            MenuItem(name:"item 2",price:11.49,special:false),
            MenuItem(name:"item 3",price:8.45,special:false),
            MenuItem(name:"item 4",price:8.45,special:false),
            MenuItem(name:"item 5",price:12.75,special:false),
            MenuItem(name:"item 6",price:13.50,special:true)
            ])
       /* add(section:"Deep Dish Pizza", item: [
            MenuItem(name:"Sausage",price:10.65,special:false),
            MenuItem(name:"Meat Lover's",price:12.35,special:false),
            MenuItem(name:"Veggie Lover's",price:10.00,special:false),
            MenuItem(name:"BBQ Chicken",price:16.60,special:true),
            MenuItem(name:"Mushroom",price:11.25,special:false),
            MenuItem(name:"Special",price:15.45,special:true)
            ])
        add(section:"Calzone", item: [
            MenuItem(name:"Sausage",price:8.00,special:false),
            MenuItem(name:"Chicken Pesto",price:8.00,special:false),
            MenuItem(name:"Prawns and Mushrooms",price:8.00,special:false),
            MenuItem(name:"Primavera",price:8.00,special:false),
            MenuItem(name:"Meatball",price:8.00,special:false)
            ])*/
    }
    
}
class listeItem: UITableViewController {
    let menuItems = MyMenuItems()
    @objc func rightButtonAction(sender: UIBarButtonItem){
        performSegue(withIdentifier: "pushScan", sender: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let rightButtonItem = UIBarButtonItem.init(
            title: "ADD",
            style: .done,
            target: self,
            action: #selector(rightButtonAction(sender:))
        )
        
        self.navigationItem.rightBarButtonItem = rightButtonItem
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return menuItems.sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
         return menuItems.sections[section].count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        let row = indexPath.row
        let section = indexPath.section
        let menuItem = menuItems.items[section][row]
        cell.textLabel?.text = menuItem.name
        let priceString = String(format:"%2.2f",menuItem.price)
        cell.detailTextLabel?.text = priceString
        return cell
        
        
        
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
           // tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }


    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
